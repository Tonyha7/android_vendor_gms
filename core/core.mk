#
# Copyright (C) 2022 Acme
#
# SPDX-License-Identifier: Apache-2.0
#

# Core
PRODUCT_SOONG_NAMESPACES += \
    vendor/gms/core

# product/etc
PRODUCT_PACKAGES += \
    privapp-permissions-google-p \
    split-permissions-google \
    google_build \
    google-hiddenapi-package-whitelist \
    product_sysconfig_google \

# product/priv-app
PRODUCT_PACKAGES += \
    Phonesky \
    PrebuiltGmsCoreSc \
    PrebuiltGmsCoreSc_AdsDynamite \
    PrebuiltGmsCoreSc_CronetDynamite \
    PrebuiltGmsCoreSc_DynamiteLoader \
    PrebuiltGmsCoreSc_DynamiteModulesA \
    PrebuiltGmsCoreSc_DynamiteModulesC \
    PrebuiltGmsCoreSc_GoogleCertificates \
    PrebuiltGmsCoreSc_MapsDynamite \
    PrebuiltGmsCoreSc_MeasurementDynamite \
    AndroidPlatformServices

# system_ext/etc
PRODUCT_PACKAGES += \
    privapp-permissions-google-se

# system_ext/priv-app
PRODUCT_PACKAGES += \
    GoogleServicesFramework
